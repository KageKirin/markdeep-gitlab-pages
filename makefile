## makefile for gitlab pages

## CUSTOMIZATION:
## - replace 'my-page' with your blog's name etc
## - replace 'myself' with your name, handle or whatever identifier you use
## - modify header.html and footer.html as needed
## - (TODO) modify CSS files in style/ and changes images as required
#


ifeq ($(UNAME_S),Darwin)
define make_date
stat -f "%Sm" -t "$(1)" $(2) >> $(3)

endef

else

define make_date
date -r $(2) "+$(1)"
date -r $(2) "+$(1)" >> $(3)

endef
endif

fmtAtomDate = "%FT%H:%M:%SZ"
fmtRssDate  = "%a, %d %b %Y %H:%M:%S %z"

define filestem
$(basename $(notdir $(1)))
endef

define make_title
$(shell echo $(call filestem,$(1)) | sed -E 's/(-)(.)/ \2/g')
endef


define test
echo 'test'
echo raw \'$(1)\'
echo filestem \'$(call filestem, $(1))\'
echo make_title \'$(call make_title, $(1))\'
ls -alG $(1)
$(call make_date, "$(fmtAtomDate)", $(1), $(2))
$(call make_date, "$(fmtRssDate)", $(1), $(2))

endef


define html_name_from_md
$(patsubst pages/%.md, $(DEPLOY)/%.html, $(1))

endef


define make_summary
cat $(1) | head -n 2 >> $(2)

endef


define make_md_index_entry
echo "## [$(call make_title,$(1))]($(call filestem, $(1)).html)" >> $(2)
echo 'posted' >> $(2)
$(call make_date, "$(fmtRssDate)", $(1), $(2))
echo '' >> $(2)
$(call make_summary, $(1), $(2))

endef


define make_atom_link
echo '<link href="https://my-page.gitlab.io/$(call filestem, $(1)).html" />' >> $(2)

endef


define make_atom_entry
echo '<entry>' >> $(2)
echo '	<title>$(call make_title,$(1))</title>' >> $(2)

$(call make_atom_link, $(1), $(2))
echo '	<id>urn:uuid:'$(shell uuidgen | tr "[:upper:]" "[:lower:]")'</id>' >> $(2)

echo '	<updated>' >> $(2)
$(call make_date, "$(fmtAtomDate)", $(1), $(2))
echo '	</updated>' >> $(2)

echo '	<summary>' >> $(2)
$(call make_summary, $(1), $(2))
echo '	</summary>' >> $(2)
echo '</entry>' >> $(2)

endef


define make_rss_entry
echo '<item>' >> $(2)
echo '	<title>$(call make_title,$(1))</title>' >> $(2)

$(call make_atom_link, $(1), $(2))
echo '	<guid isPermaLink="false">urn:uuid:'$(shell uuidgen | tr "[:upper:]" "[:lower:]")'</guid>' >> $@

echo '	<pubDate>' >> $(2)
$(call make_date, "$(fmtRssDate)", $(1), $(2))
echo '	</pubDate>' >> $(2)

echo '	<description>' >> $(2)
$(call make_summary, $(1), $(2))
echo '	</description>' >> $(2)
echo '</item>' >> $(2)

endef


sources := $(wildcard pages/*.md)
targets := $(call html_name_from_md, $(sources))

# all is default target
all: \
	$(DEPLOY) \
	$(DEPLOY)/markdeep.min.js \
	$(DEPLOY)/index.html \
	$(DEPLOY)/atom.xml \
	$(DEPLOY)/index.rss \
	$(targets) \
	;

clean:
	rm -rf $(DEPLOY)
	rm index.md

PHONY:
	echo $(shell echo 'test' | sed -e "s/e/a/g")
	echo $(UNAME)
	echo $(sources)
	echo $(targets)
	$(foreach pp, $(sources), $(call test, $(pp), $@))

# silence outputs
$(VERBOSE).SILENT:;


# deployment targets
$(DEPLOY):
	mkdir -p $(DEPLOY)


$(DEPLOY)/markdeep.min.js: $(DEPLOY)
	curl https://casual-effects.com/markdeep/latest/markdeep.min.js > $@


# HTML page conversion
$(DEPLOY)/%.html: pages/%.md $(DEPLOY)
	cat header.html $< footer.html > $@


# index page generator
index.md: $(sources)
	echo '# my-page -- Index' > $@
	$(foreach pp, $(sources), $(call make_md_index_entry, $(pp), $@))


$(DEPLOY)/index.html: index.md;
	cat header.html $< footer.html > $@


# atom feed generator
$(DEPLOY)/atom.xml: \
	$(sources) \
	;
	echo '<?xml version="1.0" encoding="utf-8"?>' > $@
	echo '<feed xmlns="http://www.w3.org/2005/Atom">' >> $@
	echo '	<title>my-page</title>' >> $@
	echo '	<link href="https://my-page.gitlab.io" />' >> $@
	echo '	<updated>' >> $@
	date "+%FT%H:%M:%SZ" >> $@
	echo '	</updated>' >> $@
	echo '	<!-- <author><name>myself</name></author> -->' >> $@
	echo '	<id>my-page.gitlab.io</id>' >> $@
	$(foreach pp, $(sources), $(call make_atom_entry, $(pp), $@))
	echo '</feed>' >> $@


# RSS 2.0 feed generator
$(DEPLOY)/index.rss: \
	$(sources) \
	;
	echo '<?xml version="1.0" encoding="utf-8"?>' > $@
	echo '<rss version="2.0">' >> $@
	echo '	<channel>' >> $@
	echo '		<title>my-page blog</title>' >> $@
	echo '		<description>something witful</description>' >> $@
	echo '		<link href="https://my-page.gitlab.io" />' >> $@
	echo '		<lastBuildDate>' >> $@
	date "+%a, %d %b %Y %H:%M:%S %z" >> $@
	echo '		</lastBuildDate>' >> $@
	echo '		<managingEditor>myself@my-page.com</managingEditor>' >> $@
	$(foreach pp, $(sources), $(call make_rss_entry, $(pp), $@))
	echo '	</channel>' >> $@
	echo '</rss>' >> $@

$(DEPLOY)/%.html: pages/%.md
	cat header.html $< footer.html > $@

