## Dockerfile to reproduce CI environment
FROM alpine:latest

RUN apk add --no-cache bash gawk sed grep bc coreutils make curl util-linux


WORKDIR /app
ADD . /app
RUN cat makefile
RUN make PHONY DEPLOY=outdir -B
RUN make DEPLOY=outdir -B

RUN cat index.md
RUN cat outdir/atom.xml
RUN cat outdir/index.rss
RUN cat outdir/*.html
